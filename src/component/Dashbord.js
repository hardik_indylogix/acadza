import React, { useState } from 'react'
import BitcoinChart from "./BitcoinChart"
import { AppBar, CssBaseline, Drawer, Hidden, IconButton, InputBase, List, Toolbar, Select, Grid, FormControl, InputLabel, MenuItem, InputAdornment, Link } from '@material-ui/core'
import { makeStyles, useTheme, fade } from '@material-ui/core/styles'
import { Menu, Search, Notifications } from '@material-ui/icons'

// Left menu(sidebar) Images
import Logo from '../img/logo.png'
import DashboardIcon from '../img/dashboard.png'
import Backlog from '../img/backlog.png'
import Rankup from '../img/rankup.png'
import Speedup from '../img/speedup.png'
import Accuracyup from '../img/accuracyup.png'
import Revision from '../img/revision.png'
import TestCreator from '../img/testCreator.png'
import AssignmentCreator from '../img/AssignmentCreator.png'
import StudyMaterial from '../img/StudyMaterial.png'
import FormulaSheet from '../img/FormulaSheet.png'
import profileImg from '../img/profileimg.png'

// Sidebar width
const drawerWidth = 320

// left sidebar menu
const leftMenus = [
	{
		id: 1,
		name: 'Dashboard',
		img: DashboardIcon
	},
	{
		id: 2,
		name: 'Backlog Remover',
		img: Backlog
	},
	{
		id: 3,
		name: 'Rank up',
		img: Rankup
	},
	{
		id: 4,
		name: 'Speed up',
		img: Speedup
	},
	{
		id: 5,
		name: 'Accuracyup',
		img: Accuracyup
	},
	{
		id: 6,
		name: 'Revision',
		img: Revision
	},
	{
		id: 7,
		name: 'Test Creator',
		img: TestCreator
	},
	{
		id: 8,
		name: 'Assignment Creator',
		img: AssignmentCreator
	},
	{
		id: 9,
		name: 'Study Material',
		img: StudyMaterial
	},
	{
		id: 10,
		name: 'Formula Sheet',
		img: FormulaSheet
	}
]

// theme custom style
const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		fontFamily: "'Poppins', sans-serif"
	},
	drawer: {
		[theme.breakpoints.up('sm')]: {
			width: drawerWidth,
			flexShrink: 0,
		},
	},
	appBar: {
		backgroundColor: '#fff',
		[theme.breakpoints.up('sm')]: {
			width: `calc(100% - ${drawerWidth}px)`,
			marginLeft: drawerWidth,
			boxShadow: 'none',
			marginTop: '15px'
		},
	},
	menuButton: {
		marginRight: theme.spacing(2),
		[theme.breakpoints.up('sm')]: {
			display: 'none',
		},
	},
	// necessary for content to be below app bar
	toolbar: theme.mixins.toolbar,
	drawerPaper: {
		width: drawerWidth,
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3),
	},
	activeStyle: {
		backgroundColor: '#5843BE',
		color: '#fff'
	},
	formControl: {
		width: '120px',
		top: '-8px',
		fontFamily: "'Poppins', sans-serif"
	},
	selectEmpty: {
		marginTop: theme.spacing(2),
	},
	search: {
		position: 'relative',
		borderRadius: theme.shape.borderRadius,
		backgroundColor: fade(theme.palette.common.white, 0.15),
		'&:hover': {
			backgroundColor: fade(theme.palette.common.white, 0.25),
		},
		marginRight: theme.spacing(2),
		marginLeft: 0,
		width: '100%',
		[theme.breakpoints.up('sm')]: {
			marginLeft: theme.spacing(3),
			width: 'auto',
		},
	},
	searchIcon: {
		padding: theme.spacing(0, 2),
		height: '100%',
		position: 'absolute',
		pointerEvents: 'none',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	icon: {
		color: "#5843be"
	}
}))

function Dashboard(props) {
	const [rightTag, setRightTag] = useState('Accuracyup')
	const [activity, setActivity] = React.useState('')
	const [toolGuide, setToolGuide] = React.useState('')
	const [profile, setProfile] = React.useState('')
	const { window } = props
	const classes = useStyles()
	const theme = useTheme()
	const [mobileOpen, setMobileOpen] = React.useState(false)

	const handleDrawerToggle = () => {
		setMobileOpen(!mobileOpen)
	}
	const handleChange = (event) => {
		setActivity(event.target.value)
	}
	const handleToolGuide = (event) => {
		setToolGuide(event.target.value)
	}
	const handleProfile = (event) => {
		setProfile(event.target.value)
	}

	const drawer = (
		<div style={{ marginTop: '30px' }}>
			<Link href="#">
				<img src={Logo} alt="site logo" style={{ maxWidth: '80%', margin: '0 auto 40px', display: 'block' }} />
			</Link>
			<List>
				{leftMenus.map((singleMenu) => (
					<li className={singleMenu.name === rightTag ? classes.activeStyle + ' item' : 'item'} onClick={() => setRightTag(singleMenu.name)} key={singleMenu.id} style={{ textAlign: 'left', padding: '10px 15px 10px 21px', cursor: 'pointer', margin: '10px 15px', borderRadius: '12px' }}>
						<img src={singleMenu.img} alt={singleMenu.name} style={{ display: 'inline-block', verticalAlign: 'middle' }} />
						<p style={{ display: 'inline-block', paddingLeft: '15px', verticalAlign: 'middle', fontSize: '19px', margin: 0, lineHeight: '30px' }} >{singleMenu.name}</p>
					</li>
				))}
			</List>
		</div>
	)

	const container = window !== undefined ? () => window().document.body : undefined

	return (
		<div className={classes.root}>
			<CssBaseline />
			<AppBar position="fixed" className={classes.appBar} >
				<Toolbar>
					<IconButton
						aria-label="open drawer"
						edge="start"
						onClick={handleDrawerToggle}
						className={classes.menuButton}
						style={{ color: '#5843be', fontSize: 40 }}
					>
						<Menu />
					</IconButton>
					<Grid container spacing={3}>
						<Grid item lg={5} xs={6} className="mobile-none">
							<div style={{ backgroundColor: '#F6F7F8', display: 'inline-block', padding: '10px 20px', borderRadius: '20px', height: '68px' }}>
								<FormControl className={classes.formControl}>
									<InputLabel id="activity">activity</InputLabel>
									<Select
										labelId="activity"
										id="activity"
										value={activity}
										classes={{
											icon: classes.icon
										}}
										onChange={handleChange}
									>
										<MenuItem value={10}>activity 1</MenuItem>
										<MenuItem value={20}>activity 2</MenuItem>
										<MenuItem value={30}>activity 3</MenuItem>
									</Select>
								</FormControl>
								<FormControl className={classes.formControl}>
									<InputLabel id="tool-guide">Tool Guide</InputLabel>
									<Select
										labelId="tool-guide"
										id="tool-guide"
										value={toolGuide}
										classes={{
											icon: classes.icon
										}}
										onChange={handleToolGuide}
									>
										<MenuItem value={10}>Tool Guide 1</MenuItem>
										<MenuItem value={20}>Tool Guide 2</MenuItem>
										<MenuItem value={30}>Tool Guide 3</MenuItem>
									</Select>
								</FormControl>
							</div>
						</Grid>
						<Grid item lg={4} xs={6} className="mobile-none">
							<div className={classes.search} style={{ backgroundColor: '#F6F7F8', display: 'inline-block', borderRadius: '15px', maxWidth: '300px', width: '100%' }}>
								<InputBase
									placeholder="Search…"
									type="search"
									startAdornment={
										<InputAdornment position="start">
											<Search />
										</InputAdornment>
									}
									classes={{
										root: classes.inputRoot,
										input: classes.inputInput
									}}
									style={{ padding: '10px 10px', height: '68px', width: '100%', color: "#5843be" }}
								/>
							</div>
						</Grid>
						<Grid item lg={3} xs={12}>
							<Link href="#">
								<img src={Logo} alt="site logo" className="mobile-logo" style={{ maxWidth: '130px', margin: '28px 0 20px', display: 'inline-block' }} />
							</Link>
							<div className="profile-area" style={{ backgroundColor: '#F6F7F8', display: 'inline-block', padding: '10px 12px', borderRadius: '15px' }}>
								<Notifications style={{ fill: '#5843BE', filter: 'drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25))', marginTop: '10px' }} />
								<FormControl className={classes.formControl} style={{ marginLeft: '0px', width: '80px', top: '-11px' }}>
									<InputLabel id="profile" style={{ top: '-10px' }}>
										<img src={profileImg} width="50px" alt="Profile" />
									</InputLabel>
									<Select
										labelId="profile"
										id="profile"
										value={profile}
										classes={{
											icon: classes.icon
										}}
										onChange={handleProfile}
									>
										<MenuItem value={10}>user 1</MenuItem>
										<MenuItem value={20}>user 2</MenuItem>
										<MenuItem value={30}>user 3</MenuItem>
									</Select>
								</FormControl>
							</div>
						</Grid>
					</Grid>
				</Toolbar>
			</AppBar>
			<nav className={classes.drawer} aria-label="mailbox folders">
				{/* The implementation can be swapped with js to avoid SEO duplication of links. */}
				<Hidden smUp implementation="css">
					<Drawer
						container={container}
						variant="temporary"
						anchor={theme.direction === 'rtl' ? 'right' : 'left'}
						open={mobileOpen}
						onClose={handleDrawerToggle}
						classes={{
							paper: classes.drawerPaper,
						}}
						ModalProps={{
							keepMounted: true, // Better open performance on mobile.
						}}
					>
						{drawer}
					</Drawer>
				</Hidden>
				<Hidden xsDown implementation="css">
					<Drawer
						classes={{
							paper: classes.drawerPaper,
						}}
						variant="permanent"
						open
					>
						{drawer}
					</Drawer>
				</Hidden>
			</nav>
			<main className={classes.content}>
				<div className={classes.toolbar} style={{ marginTop: '40px' }} />
				{rightTag === 'Accuracyup' ? <BitcoinChart /> : 'Here is nothing'}
			</main>
		</div>
	)
}

export default Dashboard