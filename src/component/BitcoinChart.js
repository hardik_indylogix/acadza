import React, { useState } from 'react'
import { Line } from "react-chartjs-2"
import { MDBContainer } from "mdbreact"
import { Grid, FormControl, NativeSelect, Select, MenuItem, InputLabel } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import bitcoinLogo from '../img/bitcoinLogo.png'

const useStyles = makeStyles((theme) => ({
	formControl: {
		width: '220px',
		top: '8px',
		backgroundColor: '#C30F70',
		color: '#fff',
		fontFamily: "'Poppins', sans-serif",
		marginTop: 0,
		height: '40px',
		margin: '0 20px',
		float: 'right',
		borderRadius: '10px'
	},
	selectEmpty: {
		marginTop: 0
	},
	icon: {
		marginTop: '4px',
		color: "#fff"
	}
}));

function BitcoinChart() {
	const classes = useStyles();
	// Default Chart Data
	var defaultData = {
		dataLine: {
			labels: [],
			datasets: [
				{
					type: "line",
					label: "BTC-USD",
					lineTension: 0.3,
					backgroundColor: "rgba(255, 255, 255, .5)",
					borderWidth: "2",
					borderColor: "#5843BE",
					pointBorderColor: "#C40F70",
					pointBackgroundColor: "#fff",
					pointBorderWidth: 3,
					data: []
				}
			]
		},
		// Chart Style
		lineChartOptions: {
			responsive: true,
			maintainAspectRatio: false,
			tooltips: {
				enabled: true
			},
			scales: {
				xAxes: [
					{
						ticks: {
							autoSkip: true,
							maxTicksLimit: 10
						}
					}
				]
			}
		}
	}

	const [bitcoin, setBitcoin] = useState(defaultData)
	const [newBtcDataSet, setNewBtcDataSet] = useState([])
	const [firstbitcoin, setFirstbitcoin] = useState()
	const [secondbitcoin, setSecondbitcoin] = useState()

	const subscribe = {
		type: "subscribe",
		channels: [
			{
				name: "ticker",
				product_ids: ["BTC-USD"]
			}
		]
	}

	// API configuration
	let bitcoinWebSocket = new WebSocket("wss://ws-feed.gdax.com")
	bitcoinWebSocket.onopen = () => {
		bitcoinWebSocket.send(JSON.stringify(subscribe))
	}

	bitcoinWebSocket.onmessage = e => {
		const value = JSON.parse(e.data)
		if (value.type !== "ticker") {
			return
		}
		const oldBtcDataSet = bitcoin.dataLine.datasets[0]
		const newBtcDataSet = { ...oldBtcDataSet }
		setNewBtcDataSet(newBtcDataSet.data.push(value.price))
		const newChartData = { ...bitcoin.dataLine, labels: bitcoin.dataLine.labels.concat(new Date().toLocaleTimeString()) }
		bitcoin.dataLine = newChartData
	}

	return (
		<MDBContainer>
			<Grid container spacing={2} alignItems="center" className="chart">
				<Grid item lg={5} xs={12}>
					<img src={bitcoinLogo} alt="Bitcoin" style={{ display: 'inline-block', verticalAlign: 'middle' }} />
					<h2 style={{ ontWeight: '500', fontSize: '32px', lineHeight: '10px', color: '#5843BE', paddingLeft: '20px', display: 'inline-block', verticalAlign: 'middle' }}>Real-time bitcoin graph</h2>
				</Grid>
				<Grid item lg={7} xs={12} justify="flex-end">

					<FormControl className={classes.formControl} style={{ backgroundColor: '#5843BE' }}>
						<InputLabel id="activity">Comodity 2</InputLabel>
						<Select
							labelId="comodity2"
							className={classes.selectEmpty}
							value={secondbitcoin}
							name="Bitcoine one"
							onChange={(e) => setSecondbitcoin(e.target.name)}
							classes={{
								icon: classes.icon
							}}
						>
							<option value={10}>Bitcoin 2.1</option>
							<option value={20}>Bitcoin 2.2</option>
							<option value={30}>Bitcoin 2.3</option>
						</Select>
					</FormControl>

					<FormControl className={classes.formControl}>
						<InputLabel id="activity">Comodity 1</InputLabel>
						<Select
							labelId="comodity"
							className={classes.selectEmpty}
							value={firstbitcoin}
							name="Bitcoine one"
							classes={{
								icon: classes.icon
							}}
							style={{ color: '#fff' }}
							onChange={(e) => setFirstbitcoin(e.target.name)}
						>
							<MenuItem value={10}>Bitcoin 1.1</MenuItem>
							<MenuItem value={20}>Bitcoin 1.2</MenuItem>
							<MenuItem value={30}>Bitcoin 1.3</MenuItem>
						</Select>
					</FormControl>
				</Grid>
			</Grid>
			<Line data={bitcoin.dataLine} options={bitcoin.lineChartOptions} height={500} />
		</MDBContainer >
	)
}

export default BitcoinChart