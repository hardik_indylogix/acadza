import React from 'react'
import Dashboard from './component/Dashbord'
import './App.css'

function App() {
	return (
		<div className="App">
			<div className="container-fluid">
				<Dashboard />
			</div>
		</div>
	)
}

export default App